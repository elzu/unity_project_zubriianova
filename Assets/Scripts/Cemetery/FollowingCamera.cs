using UnityEngine;

namespace Cemetery
{

[ExecuteInEditMode]
public class FollowingCamera : MonoBehaviour
{
  public Transform target;

  public Vector3 offset;

  void LateUpdate()
  {
    transform.position = target.position + offset;
  }
}

}
