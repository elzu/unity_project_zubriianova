﻿using System;
using UnityEngine;
using UnityEngine.AI;

namespace Cemetery
{
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class Hero : MonoBehaviour
{
  #region Properties
  
      public Vector3? point = null;
      
      public float HpMax = 100.0f;
      
      public float regen_hp_speed = 0.2f;
      public float regen_hp_time = 10.0f;
      public int FanSize = 7; 
      
      public float xp = 0.0f;
      public float ShootSpeed = 1f;
      public float shot_target_distance = 30f;
      public float change_target_distance = 15f;
      
      public GameObject bomb;
    
  #endregion
  
  NavMeshAgent agent;
  Animator animator;

  public float Hp;
  private float ShootTimer = 0.0f;
  
  public Unit distance_target = null;
  private bool fired = false;
  private bool castBomb = false;
  private bool castFan = false;
  
  public enum State
  {
    idle,
    walk,
    die,
    win
  }

  float last_hit_time = 0.0f;
  public State current_state = State.idle;
  private float angular_speed;

  void Start()
  {
    animator = GetComponent<Animator>();
    agent = GetComponent<NavMeshAgent>();
    angular_speed = agent.angularSpeed;
    Hp = HpMax;
  }

  public void ResumeHp()
  {
     HpMax = HpMax + (G.self.GameLevel - 1)*50;
     Hp = HpMax;
  }
  
  public void SetTarget(Vector3 point)
  {
    this.point = point;
  }

  public void SetDistanceTarget(Unit target)
  {
    point = null;
    this.distance_target = target;
  }
  
  public void SetFire(bool fire)
  {
    fired = fire;
  } 
  
  public void SetBomb()
  {
    castBomb = true;
  }
  
  public void SetFan()
  {
    castFan = true;
  }
  
  public void MakeShot()
  {
    if (!fired)
      return;
    
    if(ShootTimer<=0)
    {
      Unit shot_target = GetShotTarget();
      //Debug.Log("current_state " + current_state);

      if (shot_target != null)
      {
        RoundToTarget(shot_target);
      }

      OneShot(Vector3.zero, G.self.BulletPool);
      ShootTimer = ShootSpeed;
    }
    else
    {
      ShootTimer -= Time.deltaTime ;
    }
  }

  private void OneShot(Vector3 deviation, ObjectPool source)
  {
    var bulletInstance = (Bullet) source.GetObject(new Vector3(transform.position.x, transform.position.y + 3f, transform.position.z), transform.rotation).GetComponent<Bullet>();

    //bulletInstance.transform.rotation = transform.rotation;
    //var bulletInstance = (Bullet) Instantiate(bullet, new Vector3(transform.position.x,transform.position.y+2f, transform.position.z), transform.rotation);
    bulletInstance.source = gameObject;
    bulletInstance.rigidBody.AddForce((transform.forward + deviation) * bulletInstance.force);
  }
  
  public void CastBomb()
  {
    if (!castBomb)
      return;
    castBomb = false;
      Instantiate(bomb, new Vector3(transform.position.x,transform.position.y+2f, transform.position.z), transform.rotation);
  }
  
  public void CastFan()
  {
    if (!castFan)
      return;
    castFan = false;
    Unit shot_target = GetShotTarget();
    //Debug.Log("current_state " + current_state);

    if (shot_target != null)
    {
      RoundToTarget(shot_target);
    }
    for (var i = 0; i < FanSize; i++)
    {
      OneShot(Vector3.left*(i - FanSize/2)/4, G.self.FanBulletPool);
    }
    
  }

  public void MoveToPoint()
  {
    if(point == null)
      return;

    SetDestination(point.Value);

    if ((transform.position - point.Value).magnitude < 1.0f)
    {
      point = null;
    }
  }

  void SetDestination(Vector3 destination)
  {
    PlayAnimation(State.walk);

    if (fired && distance_target != null)
    {
      agent.angularSpeed = 0;
    }
    else
    {
      agent.angularSpeed = angular_speed;
    }
    agent.SetDestination(destination);
  }
  private Unit GetShotTarget()
  {
    //if (distance_target == null)
    //{
      distance_target = G.self.FindNearestEnemies(shot_target_distance);
    //}
   /* else
    {
      var alter_distance_target = G.self.FindNearestEnemies(change_target_distance);
      if (alter_distance_target != null)
      {
        distance_target = alter_distance_target;
      }
    }*/

    return distance_target;
  } 


  void RoundToTarget(Unit roundTarget)
  {
    Vector3 direction = (roundTarget.transform.position - transform.position).normalized;
    Quaternion lookRotation = Quaternion.LookRotation(direction);
    transform.rotation = Quaternion.RotateTowards(transform.rotation, lookRotation, 360);
  }
  
  public void Hit(Unit attacker)
  {
      Hit(attacker, attacker.attack);
  }
  
  public void Hit(Unit attacker, float damage)
  {
    Hp -= damage;
    last_hit_time = Time.time;
  }

  public void AddXP(float xp)
  {
    this.xp += xp;
  }

  void PlayAnimation(State state)
  {
    if(current_state == state)
      return;
    
    animator.SetBool("fired", fired);
    animator.ResetTrigger(current_state.ToString());
    current_state = state;
    animator.SetTrigger(state.ToString());
  }

  void RegenHP()
  {
    if(last_hit_time + regen_hp_time > Time.time)
      return;

    Hp += regen_hp_speed * Time.deltaTime;
    if(Hp > HpMax)
      Hp = HpMax;
  }
  void FixedUpdate()
  {
    animator.SetFloat("speed_walk", agent.velocity.magnitude / agent.speed);

     MoveToPoint();
     CastBomb();
     CastFan();
     MakeShot();

    if(point == null)
    {
      PlayAnimation(State.idle);
    }
    
    RegenHP();
  }
}

}
