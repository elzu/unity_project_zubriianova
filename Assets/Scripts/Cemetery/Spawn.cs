﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cemetery;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public int Id;
    public float SpawnPeriod = 7f;
    public float MinSpawnPeriod = 3f;
    public int MaxSpawnCount = 5;
    public int AddSpawnCountPerLevel = 2;
    public bool StopSpawn = false;
    public float SpawnPeriodPerLevel = 0.05f;
    public int Count = 0;
    public int MaxCount = 300;
    public int SwitchOnLevel = 1;
    
    public float CurrentSpawnPeriod {
        get
        {
            var period = (1 - G.self.GameLevel * SpawnPeriodPerLevel) * SpawnPeriod;
            return (period < MinSpawnPeriod)?MinSpawnPeriod:period;
        }
    }    


    public Unit SpawnUnit;
    private float SpawnTimer = 0.0f;
    private int LevelMaxSpawnCount => MaxSpawnCount + AddSpawnCountPerLevel * G.self.GameLevel;
    
    void Start()
    {
        
    }

    public void Init(int Level)
    {
        StopSpawn = SwitchOnLevel > Level;
    }
    
    public void CloseLowLevel(int Level)
    {
        StopSpawn = SwitchOnLevel != Level;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        DoSpawn();
    }

    private int GetActiveEnemyCount()
    {
        return G.self.enemies.Count(e => e.spawn_id == Id);
    }
    private void DoSpawn()
    {
        if (StopSpawn) return;
        if (GetActiveEnemyCount() >= LevelMaxSpawnCount) return;
        if (Count >= MaxCount) return;
        
        if(SpawnTimer <= 0)
        {
            var unit = (Unit) Instantiate(SpawnUnit, new Vector3(transform.position.x,transform.position.y, transform.position.z), transform.rotation);
            unit.spawn_id = Id;
            unit.id = Count;
            Count++;
            unit.gameObject.SetActive(true);
            unit.SetTarget(G.self.player);
            SpawnTimer = CurrentSpawnPeriod;
        }
        else
        {
            SpawnTimer -= Time.deltaTime ;
        }
    }
}
