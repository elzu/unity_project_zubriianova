﻿using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

namespace Cemetery
{
    public class ObjectPool: MonoBehaviour
    {
        public GameObject Prototype;
        public List<GameObject> pool = new List<GameObject>();
        public int created = 0;
        public int poolSize => pool.Count;
        public GameObject GetObject(Vector3 position, Quaternion rotation)
        {
            lock (pool)
            {
                GameObject obj;
                if (pool.Count > 0)
                {
                    obj = pool[0];
                    pool.RemoveAt(0);
                }
                else
                {
                    obj = CreateObject();
                }
                obj.GetComponent<PoolingObject>().ParentPool = this;
                obj.SetActive(true);
                obj.transform.position = position;
                obj.transform.eulerAngles = Prototype.transform.eulerAngles;
                obj.transform.rotation = rotation;
                if (obj.GetComponent<Rigidbody>() != null) {
                    obj.GetComponent<Rigidbody>().velocity = Vector3.zero;
                    obj.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                }
                return obj;
            }
        }
        
      

        private GameObject CreateObject()
        {
            created++;
            //Debug.Log("created " + created);
            return GameObject.Instantiate(Prototype);
        }
        
        public void ReturnObject(GameObject gameObject)
        {
            lock (pool)
            {  
                gameObject.SetActive(false);
               pool.Add(gameObject);
              // Debug.Log(" Enqueue pool " + pool.Count);

            }
        }
    }
}