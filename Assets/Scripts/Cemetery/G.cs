using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Cemetery
{

public class G : MonoBehaviour
{
  public static G self = null;

  public Hero player;
  
  private Joystick joystick;
  private Firebutton firebutton;
  public MagicButton bombButton;
  public MagicButton fanButton;

  public SimpleHealthBar HealthProgress;
  public SimpleHealthBar LevelProgress;
  public Text LevelInfo;

  public List<Unit> enemies;
  public List<Spawn> spawns;

  public ObjectPool BulletPool;
  public ObjectPool FanBulletPool;
  public ObjectPool ExplosionPool;
  
  public Canvas GameCanvas;
  public Canvas GameOverCanvas;
  public Canvas GameWinCanvas;
  public Canvas GameStartCanvas;
  public Canvas ConversationCanvas;

  public float TargetCountStart = 30f;
  public float TargetCountPerLevel = 0.2f;
  public int MaxGameLevel = 3;
  public int TargetCount;
  public int GameLevel = 0;
  public int KilledCount;
  public int LastLevel = 4;

  public bool ShowFirstPage = true;
  
  private void Start()
  {
    Time.timeScale = 1.0f;

    Debug.Log("Start");
    joystick = FindObjectOfType<Joystick>();
    firebutton = FindObjectOfType<Firebutton>();
    enemies = new List<Unit>();
    spawns = FindObjectsOfType<Spawn>().ToList();
    StartNextLevel();
  }
  
  private void StartNextLevel()
  {
    if ((GameLevel + 1) == LastLevel)
    {
      spawns.ForEach(s=>s.CloseLowLevel(GameLevel+1));
    }
    else
    {
      GameLevel++;
      spawns.ForEach(s => s.Init(GameLevel));
      TargetCount = Convert.ToInt32(TargetCountStart + TargetCountStart * TargetCountPerLevel * (GameLevel - 1));
      KilledCount = 0;
    }
    //player.ResumeHp();
      
  }

  public void RegisterEnemy(Unit enemy)
  {
    enemies.Add(enemy);
    //Debug.Log("RegisterEnemy - count after - "+enemies.Count);
  }
  
  public void UnRegisterEnemy(Unit enemy)
  {
    enemies.Remove(enemy);
    KilledCount++;
  }
  

  void Awake()
  {
    if(self == null)
      self = this;
  }

  void Update()
  {
    SetPlayerState();
    FillCanvas();
    CheckGameOver();
    CheckGameWin();
    CheckNextLevel();
  }

  void SetPlayerState()
  {
    player.SetTarget(player.transform.position +  new Vector3(joystick.Horizontal*5f, 0, joystick.Vertical*5f));
    player.SetFire(firebutton.Pressed || Input.GetKey("space"));

    if (bombButton.DoCast)
    {
      player.SetBomb();
      bombButton.Reset();
    }
    
    if (fanButton.DoCast)
    {
      player.SetFan();
      fanButton.Reset();
    }
  }

  void FillCanvas()
  {
    HealthProgress.UpdateBar(player.Hp, player.HpMax);
    LevelProgress.UpdateBar(KilledCount, TargetCount);
    LevelInfo.text = $"Уровень: {GameLevel}/{MaxGameLevel}";
  }

  void CheckGameOver()
  {
    if (player.Hp <= 0)
    {
      Time.timeScale = 0.0f;
      GameCanvas.gameObject.SetActive(false);
      GameOverCanvas.gameObject.SetActive(true);
    }
  } 
  
  void CheckGameWin()
  {
    if ((GameLevel+1) == LastLevel && enemies.Count == 0)
    {
      StartCoroutine( WinCoroutine());
    }
  }
  
  IEnumerator WinCoroutine()
  {
    yield return new WaitForSeconds(3);
    Time.timeScale = 0.0f;
    GameCanvas.gameObject.SetActive(false);
    GameWinCanvas.gameObject.SetActive(true);
  }
  
  void CheckNextLevel()
  {
    if(KilledCount >= TargetCount)
      StartNextLevel();
  }

  public Unit FindNearestEnemies(float maxCheckDistance)
  {
    Unit result = null;
    float minDistance = maxCheckDistance;
    
    var allEnemies = GameObject.FindGameObjectsWithTag("Enemy");

    foreach (var enemy in allEnemies)
    {
      Vector3 diff = player.transform.position - enemy.transform.position;
      var curDistance = diff.magnitude;

      if (curDistance < minDistance)
      {
        result = enemy.GetComponent<Unit>();
        minDistance = curDistance;
      }
    }
    return result;
  }
 
}

}
