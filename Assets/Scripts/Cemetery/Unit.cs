﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace Cemetery
{

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class Unit : MonoBehaviour
{
  private static float allow_patrol_distance = 1.5f;
  
  NavMeshAgent agent;
  Animator animator;

  public Hero target;
  public Vector3? point = null;
  public SimpleHealthBar simpleHealthBar; 
  
  public Vector3 start_point;

  public float hp;
  public float hp_max = 100.0f;
  public float attack_radius = 4.0f;

  public float attack = 5.0f;

  public float attack_per_level = 1.0f;

  public bool in_attack = false;

  public float attack_per_second = 2.0f;

  public float regen_hp_speed = 10.0f;
  public float regen_hp_time = 2.0f;

  public float last_attack;

  bool wait_attack_time;

  public bool is_patrol = false;
  public GameObject patrol_point;

  public bool hide_on_start = false;
  
  public Unit distance_target = null;
  private bool fired = false;
  public int spawn_id;
  public int id;

  [HideInInspector]
  public Rigidbody rigidBody;
    
  public enum State
  {
    idle,
    walk,
    attack,
    fire,
    die,
    win
  }

  float last_hit_time = 0.0f;

  public State current_state = State.idle;

  IEnumerator attack_coroutine = null;

  private float angular_speed;

  void Start()
  {
    simpleHealthBar = GetComponentInChildren<SimpleHealthBar>();
    rigidBody = GetComponent<Rigidbody>();

    animator = GetComponent<Animator>();

    agent = GetComponent<NavMeshAgent>();
    angular_speed = agent.angularSpeed;

    
    hp = (float) (hp_max+hp_max*0.1*G.self.GameLevel);
    
    start_point = transform.position;
    

    if (hide_on_start)
    {
      Hide();
    }
  }

  private void Hide()
  {
    gameObject.SetActive(false);
  }
  
  public void Show()
  {
    gameObject.SetActive(true);
  }
  
  public void SetTarget(Hero target)
  {
    point = null;
    this.target = target;
  }
  
  public void SetDistanceTarget(Unit target)
  {
    point = null;
    this.distance_target = target;
  }
  
  public void SetTarget(Vector3 point)
  {
    this.point = point;
    target = null;
    //StopAttack();
  }

  Vector3 GetTargetPosition()
  {
    if(target == null)
      return Vector3.zero;

    return (transform.position - target.transform.position).normalized * (attack_radius / 2.0f) + target.transform.position;
  }

  public void MoveToPoint()
  {
    if(target != null)
      return;

    if(point == null)
      return;

    SetDestination(point.Value);

    if ((transform.position - point.Value).magnitude < 1.0f)
    {
      Debug.Log((transform.position - point.Value).magnitude.ToString());
      point = null;
    }
  }

  void StopAttack()
  {
    if(attack_coroutine != null)
      StopCoroutine(attack_coroutine);
    in_attack = false;
  }

  void SetDestination(Vector3 destination)
  {
    PlayAnimation(State.walk);

    if (fired && distance_target != null)
    {
      agent.angularSpeed = 0;
    }
    else
    {
      agent.angularSpeed = angular_speed;
    }
    agent.SetDestination(destination);
  }

  void MoveToTarget()
  {
    if(target == null || in_attack)
      return;

    if(!IsTargetInAttackRadius())
      SetDestination(GetTargetPosition());
    else
      agent.ResetPath();
  }

  bool IsTargetInAttackRadius()
  {
    return target != null && Mathf.Abs((target.transform.position - transform.position).magnitude) <= attack_radius;
  }

  void AttackTarget()
  {
    wait_attack_time = (Time.time - last_attack) < attack_per_second;
    if(IsTargetInAttackRadius() && !in_attack && !wait_attack_time)
    {
       attack_coroutine = Attack();
       StartCoroutine(attack_coroutine);
       last_attack = Time.time;
    }
  }

  
  IEnumerator LookToTarget()
  {
    Vector3 direction = (target.transform.position - transform.position).normalized;
    Quaternion lookRotation = Quaternion.LookRotation(direction);

    if(lookRotation != transform.rotation && IsTargetInAttackRadius())
      PlayAnimation(State.walk);
    else
      yield break;

    while(lookRotation != transform.rotation && IsTargetInAttackRadius())
    {
      direction = (target.transform.position - transform.position).normalized;
      lookRotation = Quaternion.LookRotation(direction);

      transform.rotation = Quaternion.RotateTowards(transform.rotation, lookRotation, Time.deltaTime * agent.angularSpeed);
      yield return null;
      if(!IsTargetInAttackRadius())
        break;
    }
  }
  
  
  void MakeHit()
  {
    if (IsTargetInAttackRadius())
      target.Hit(this);
   // wait_attack_time = false;
  }

  IEnumerator Attack()
  {
    animator.SetFloat("speed_attack", attack_per_second);

    yield return LookToTarget();

    in_attack = true;

    wait_attack_time = IsTargetInAttackRadius();
    if (wait_attack_time)
    {
      PlayAnimation(State.attack);
      MakeHit();
    }

    while (wait_attack_time)
      yield return null;

    if(!IsTargetInAttackRadius())
      PlayAnimation(State.idle);

    in_attack = false;
  }
  
 
  public float radius = 5.0F;
  public float power = 10.0F;
  public void Hit(Hero attacker, float damage)
  {
    hp -= damage;
    last_hit_time = Time.time;
    if(target == null && point == null)
      SetTarget(attacker);
    
    if(simpleHealthBar != null)
    simpleHealthBar.UpdateBar(hp, hp_max);

    if(hp <= 0)
    {
      Destroy(gameObject);
      if(attacker.distance_target != null && attacker.distance_target.gameObject == gameObject)
        attacker.distance_target = null;
    }
  }

  void PlayAnimation(State state)
  {
    if(current_state == state)
      return;

    animator.ResetTrigger(current_state.ToString());
    current_state = state;
    animator.SetTrigger(state.ToString());
  }

  void RegenHP()
  {
    if(last_hit_time + regen_hp_time > Time.time)
      return;

    hp += regen_hp_speed * Time.deltaTime;
    if(hp > hp_max)
      hp = hp_max;
  }

  private bool DoPatrol()
  {
    if (is_patrol && patrol_point != null && target == null && point == null)
    {
      var dis1 = Vector3.Distance(transform.position, start_point); 
      var dis2 = Vector3.Distance(transform.position, patrol_point.transform.position); 
      if(dis1 < allow_patrol_distance) 
      {
        SetTarget(patrol_point.transform.position);
      }
      else if (dis2 <= allow_patrol_distance)
      {
        SetTarget(start_point);
      }
      MoveToPoint();
      return true;
    }

    return false;
  }

  void Update()
  {
    animator.SetFloat("speed_walk", agent.velocity.magnitude / agent.speed);

     MoveToPoint();
     MoveToTarget();
     AttackTarget();
     
    if(target == null && point == null)
    {
      if(!DoPatrol())
      {
        PlayAnimation(State.idle);
      }
      in_attack = false;
    }
    
    RegenHP();
  }
}

}
