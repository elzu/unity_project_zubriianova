using UnityEngine;

namespace Cemetery
{

[RequireComponent(typeof(Unit))]
public class Enemy : MonoBehaviour
{
  void Start()
  {
    G.self.RegisterEnemy(gameObject.GetComponent<Unit>());
  }

  void OnDestroy()
  {
    G.self.UnRegisterEnemy(gameObject.GetComponent<Unit>());  
  }

  
}

}
