﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Cemetery
{
  public class Dialogue : MonoBehaviour, IPointerClickHandler
  {
    [Serializable]
    public class Page
    {
      public string name;
      public string message;
      public Sprite left;
      public Sprite right;
    }
  
    public List<Page> pages;
    private Text title;
    private Text message;
    private Image left;
    private Image right;
    
    private void Start()
    {
      title = FindObjectsOfType<Text>().FirstOrDefault(o=>o.name.Equals("title"));
      message = FindObjectsOfType<Text>().FirstOrDefault(o=>o.name.Equals("message"));
      left = FindObjectsOfType<Image>().FirstOrDefault(o=>o.name.Equals("left"));
      right = FindObjectsOfType<Image>().FirstOrDefault(o=>o.name.Equals("right"));
      
      NextPage();
    }

    private int currentPage = -1;

    private void NextPage()
    {
      currentPage++;
      if (pages.Count <= currentPage)
      {
        CompliteDialog();
      }

      Page page = pages[currentPage];
      title.text = page.name;
      message.text = page.message;
      
      left.sprite = page.left;
      var leftColor = left.color;
      leftColor.a = left.sprite == null?0f:1f;
      left.color = leftColor;
      
      right.sprite = page.right;
      var rightColor = right.color;
      rightColor.a= right.sprite == null?0f:1f;
      right.color = rightColor;
    }

    public void CompliteDialog()
    {
      SceneManager.LoadScene("Cemetery");
    }
    
   public void OnPointerClick(PointerEventData eventData)
   {
     NextPage();
   }
  }
}