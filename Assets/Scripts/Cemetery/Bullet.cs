﻿using System;
using UnityEditor;
using UnityEngine;

namespace Cemetery
{
    public class Bullet : MonoBehaviour
    {
        public float damage = 20f;
        public float damagePerLevel = 0.2f;
        public float force = 3000f;
        public float destroyDistance = 100f;
        public AudioClip shot;
        public AudioClip fanShot;
        private AudioSource audioSource;
        public ObjectPool ParentPool;
        
        [HideInInspector]
        public Rigidbody rigidBody;
        [HideInInspector]
        public GameObject source;

        private Vector3 startPoint;

        private float CurrentDamage => (1 + damagePerLevel * G.self.GameLevel) * damage;
        void Start()
        {
            rigidBody = GetComponent<Rigidbody>();
            audioSource = GetComponent<AudioSource>();
            startPoint = transform.position;
        }

        private void OnEnable()
        {
            startPoint = transform.position;
            //if(audioSource != null)
                //audioSource.Play();
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Enemy"))
            {
                var explosion = G.self.ExplosionPool.GetObject(
                    new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation);
                explosion.GetComponent<Explosion>().SetInstance();
                
                //Instantiate(explosion, new Vector3(transform.position.x,transform.position.y, transform.position.z), transform.rotation).SetInstance();
                MakeHit(other.gameObject);
                GetComponent<PoolingObject>().ParentPool.ReturnObject(this.gameObject);
                //Destroy(this.gameObject); 
            }  
        }
        
        private void Update()
        {
            Vector3 diff = transform.position - startPoint;
            var curDistance = diff.magnitude;
            if (curDistance > destroyDistance)
            {
                GetComponent<PoolingObject>().ParentPool.ReturnObject(this.gameObject);
            }
        }

        public void MakeHit(GameObject target)
        {
            var enemy = target.GetComponent<Unit>();
            enemy.Hit(this.source.GetComponent<Hero>(), CurrentDamage);
        }
    }
}
