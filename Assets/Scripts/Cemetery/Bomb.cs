﻿using System.Collections.Generic;
using UnityEngine;

namespace Cemetery
{
    public class Bomb : MonoBehaviour
    {
        public float damage = 20f;
        public float damagePerLevel = 0.2f;

        public ParticleSystem part;
        public List<ParticleCollisionEvent> collisionEvents;
        
        private float CurrentDamage => (1 + damagePerLevel * (G.self.GameLevel-1)) * damage;

        // Start is called before the first frame update
        void Start()
        {
            part = GetComponent<ParticleSystem>();
            collisionEvents = new List<ParticleCollisionEvent>();
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    
        public void OnParticleSystemStopped()
        {
            //Debug.Log("System has stopped!");        
        }

        public void MakeHit(GameObject target)
        {
            var enemy = target.GetComponent<Unit>();
            enemy.Hit(G.self.player, CurrentDamage);
        }

        void OnParticleCollision(GameObject other)
        {
            if (other.gameObject.CompareTag("Enemy"))
            {
                Unit unit = other.gameObject.GetComponent<Unit>();
                Debug.Log($"Boom! {unit.spawn_id}_{unit.id}"+other.name);

                
                var explosion = G.self.ExplosionPool.GetObject(new Vector3(other.transform.position.x, other.transform.position.y, other.transform.position.z), other.transform.rotation);
                explosion.GetComponent<Explosion>().SetInstance();
                MakeHit(other.gameObject);
            }  
        
            //TODO Отбрасывание
            /* int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);

        Rigidbody rb = other.GetComponent<Rigidbody>();
        int i = 0;

        while (i < numCollisionEvents)
        {
            if (rb)
            {
                Vector3 pos = collisionEvents[i].intersection;
                Vector3 force = collisionEvents[i].velocity * 10;
                rb.AddForce(force);
            }
            i++;
        }*/
        }
    }
}
