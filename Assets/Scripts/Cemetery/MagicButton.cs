﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Cemetery
{
    public class MagicButton : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
    {
        public bool Pressed;
        public Color DefaultColor;
        public Color PressedColor;

        public float RestorationTime = 5.0f;
        public Image ButtonImage;

        public bool IsActive;

        private float timer;
        private Image image;
        
        public bool DoCast => Pressed && IsActive;

        private void Start()
        {
            image = GetComponent<Image>();
            image.color = DefaultColor;
            Reset();
        }

        public void Reset()
        {
            IsActive = false;
            timer = RestorationTime;
            image.color = DefaultColor;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Pressed = false;
        }
        public void OnPointerDown(PointerEventData eventData)
        {
            Pressed = true;
        }

        private void FixedUpdate()
        {
            if(timer<=0)
            {
                if (!IsActive)
                {
                    IsActive = true;
                    image.color = PressedColor;
                }
            }
            else
            {
                timer -= Time.deltaTime ;
                image.fillAmount = 1- timer / RestorationTime;
                ButtonImage.fillAmount= 1- timer / RestorationTime;
            }
        }
    }
}