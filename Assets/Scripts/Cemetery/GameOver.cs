﻿using Cemetery;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour, IPointerClickHandler
{
    public void StartFirstPage()
    {
        Debug.Log("GameOver StartFirstPage");
        SceneManager.LoadScene("Start");
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        StartFirstPage();
    }
}
