﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Cemetery
{
    public class Firebutton : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
    {
        public bool Pressed;
        public Color PressedColor;
        public Color DefaultColor;

        private Image image;

        private void Start()
        {
            image = GetComponent<Image>();
            image.color = DefaultColor;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Pressed = false;
            image.color = DefaultColor;
        }
        public void OnPointerDown(PointerEventData eventData)
        {
            Pressed = true;
            image.color = PressedColor;
        }
    }
}
