﻿using System.Collections;
using System.Collections.Generic;
using Cemetery;
using UnityEditor;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    private ParticleSystem explosionParticleSystem;
    private AudioSource audioSource; 
    private bool IsInstance { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        explosionParticleSystem = GetComponent<ParticleSystem>();
        if (IsInstance)
        {
            audioSource = GetComponent<AudioSource>();
            var mainModule = explosionParticleSystem.main;
            mainModule.stopAction = ParticleSystemStopAction.Callback;
        }
    }

    public void SetInstance()
    {
        IsInstance = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (explosionParticleSystem.isStopped)
        {
            if (audioSource != null)
            {
                audioSource.Play();
            }
            explosionParticleSystem.Play();
        }
    }
    
    public void OnParticleSystemStopped()
    {
        G.self.ExplosionPool.ReturnObject(gameObject);
    }
}
