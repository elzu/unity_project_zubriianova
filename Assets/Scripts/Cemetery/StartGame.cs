﻿using System.Linq;
using Cemetery;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    public bool SkipDialogue = false;

    public Canvas dialogue;
    public Canvas start;
    
    
    
    public void StartScene()
    {
        if (SkipDialogue)
        {
            Debug.Log("Start Cemetery");
            SceneManager.LoadScene("Cemetery");
        }
        else
        {
            dialogue.gameObject.SetActive(true);
            start.gameObject.SetActive(false);
        }
    }
}
